#include "alarm_list.h"
#include <gtest/gtest.h>

class BufferTestHandler : public SchedEvent
{
public:
	~BufferTestHandler(){};

	int counter = 0;

	void action()
	{
		counter++;
	}
};

static BufferTestHandler head_cntr;
class Buffer : public ::testing::Test
{
protected:

	AlarmBuffer buffer_a;
	AlarmBuffer buffer_b;
	AlarmPtr a_ptr;
	void SetUp()
	{
		Alarm ex(5,6,(sched::day_t)2, head_cntr);
		buffer_b.clone_alarm(a_ptr, ex);

	}
	void TearDown()
	{
	}
};



TEST_F(Buffer, happy)
{
	Alarm example(1,2,(sched::day_t)3, head_cntr);

	AlarmPtr ptr ;

	ASSERT_EQ(true, buffer_a.clone_alarm(ptr, example));
	ASSERT_EQ(1, ptr->time.hour);

	for (int i = 0; i < buffer_a.size-1; ++i)
	{
		ASSERT_EQ(true, buffer_a.clone_alarm(ptr, example));
	}

	ASSERT_EQ(false, buffer_a.clone_alarm(ptr, example));
}

TEST_F(Buffer, get_cursor_happy)
{
	Alarm example(1,2,(sched::day_t)3, head_cntr);

	AlarmPtr ptr ;

	for (int i = 0; i < buffer_a.size; i+=2)
	{
		buffer_a.mem[i] = true;
	}

	ASSERT_EQ(true, buffer_a.clone_alarm(ptr, example));
	ASSERT_EQ(1, ptr->time.hour);

	for (int i = 0; i < buffer_a.size /2 - 1; ++i)
	{
		ASSERT_EQ(true, buffer_a.clone_alarm(ptr, example));
	}

	ASSERT_EQ(false, buffer_a.clone_alarm(ptr, example));
}

TEST_F(Buffer, delete_sym)
{
	ASSERT_EQ(1, buffer_b.get_cursor());
	ASSERT_EQ(true, buffer_b.delete_alarm(a_ptr));
	ASSERT_EQ(0, buffer_b.get_cursor());
}
