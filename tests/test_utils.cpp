/*
 * test_utils.cpp
 *
 *  Created on: Jan 2, 2020
 *      Author: misha
 */

#include "time_utils.h"
#include <gtest/gtest.h>



TEST(TimeRealationalMore, more_hour)
{
	sched::Time less(10,15, sched::MON);
	sched::Time more(12,15, sched::MON);
	ASSERT_EQ(true, more > less);
	ASSERT_EQ(false, less > less);
}

TEST(TimeRealationalMore, more_min)
{
	sched::Time less(10,15, sched::MON);
	sched::Time more(10,19, sched::MON);
	ASSERT_EQ(true, more > less);
	ASSERT_EQ(false, less > less);
}


TEST(TimeRealationalMore, more_eq)
{
	sched::Time less(10,15, sched::MON);
	sched::Time more(10,15, sched::MON);
	ASSERT_EQ(false, more > less);
	ASSERT_EQ(false, less > less);
}


TEST(TimeRelationalEqual, eq)
{
	sched::Time eq1(10,15, sched::MON);
	sched::Time eq2(10,15, sched::MON);

	sched::Time more(12,15, sched::MON);
	sched::Time less(0,15, sched::MON);

	ASSERT_EQ(true, eq2 == eq1);
	ASSERT_EQ(true, eq1 == eq1);

	ASSERT_EQ(false, eq2 == more);
	ASSERT_EQ(false, eq1 == less);
}


sched::Time eq1(10,15, sched::MON);
sched::Time eq2(10,15, sched::MON);

sched::Time more(12,15, sched::MON);
sched::Time less(0,15, sched::MON);

TEST(TimeRelationalMoreEqual, eq)
{
	ASSERT_EQ(true, eq2 >= eq1);
	ASSERT_EQ(true, eq1 >= eq1);
}

TEST(TimeRelationalMoreEqual, more)
{
	ASSERT_EQ(false, eq2 >= more);
}

TEST(TimeRelationalMoreEqual, less)
{
	ASSERT_EQ(true, eq1 >= less);
}

///===================================///
TEST(TimeRelationalLess, eq)
{
	ASSERT_EQ(false, eq2 < eq1);
	ASSERT_EQ(false, eq1 < eq1);
}

TEST(TimeRelationalLess, more)
{
	ASSERT_EQ(true, eq2 < more);
}

TEST(TimeRelationalLess, less)
{
	ASSERT_EQ(false, eq1 < less);
}

///===================================///
TEST(TimeRelationalLessEq, eq)
{
	ASSERT_EQ(true, eq2 <= eq1);
	ASSERT_EQ(true, eq1 <= eq1);
}

TEST(TimeRelationalLessEq, more)
{
	ASSERT_EQ(true, eq2 <= more);
}

TEST(TimeRelationalLessEq, less)
{
	ASSERT_EQ(false, eq1 <= less);
}
