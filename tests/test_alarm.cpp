/*
 * test_alarm.cpp
 *
 *  Created on: Jan 2, 2020
 *      Author: misha
 */
#include "alarm_list.h"
#include <gtest/gtest.h>

#include "alarm_handler.hpp"


static AlarmTest head_cntr;
static AlarmTest lunch_cntr;
static AlarmTest dinner_cntr;
static AlarmTest tea_cntr;

Alarm head_alarm(10, 15, sched::MON, head_cntr);
Alarm lunch(14, 0, sched::MON, lunch_cntr);
Alarm dinner(18, 35, sched::MON, dinner_cntr);
Alarm tea_time(22, 15, sched::MON, tea_cntr);


class AlarmInsert : public ::testing::Test
{
protected:

	void SetUp()
	{
		head_alarm = Alarm (10, 15, sched::MON, head_cntr);
		lunch = Alarm (14, 0, sched::MON, lunch_cntr);
		dinner = Alarm (18, 35, sched::MON, dinner_cntr);
		tea_time = Alarm (22, 15, sched::MON, tea_cntr);
		head_cntr.counter = 0;
		lunch_cntr.counter = 0;
		dinner_cntr.counter = 0;
		tea_cntr.counter = 0;
	}
	void TearDown()
	{
	}
};

TEST_F(AlarmInsert, happy_sorted_input)
{
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, lunch));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, dinner));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, tea_time));
	ASSERT_EQ(true, head_alarm.next == &lunch);
	ASSERT_EQ(true, lunch.next == &dinner);
	ASSERT_EQ(true, dinner.next == &tea_time);
}

TEST_F(AlarmInsert, happy_unsorted)
{
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, dinner));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, tea_time));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, lunch));

	ASSERT_EQ(true, head_alarm.next == &lunch);
	ASSERT_EQ(true, lunch.next == &dinner);
	ASSERT_EQ(true, dinner.next == &tea_time);
}

TEST_F(AlarmInsert, remove_unhappy)
{
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, dinner));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, tea_time));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, lunch));

	ASSERT_EQ(true, AlarmList::remove(head_alarm, lunch));
	ASSERT_EQ(false, AlarmList::remove(head_alarm, lunch));

}

TEST_F(AlarmInsert, remove_happy)
{
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, dinner));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, tea_time));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, lunch));

	ASSERT_EQ(true, AlarmList::remove(head_alarm, lunch));

	sched::Time time = sched::Time(23, 59, sched::MON);
	AlarmList::checkList(head_alarm, time);

	ASSERT_EQ(1, head_cntr.counter);
	ASSERT_EQ(0, lunch_cntr.counter);
	ASSERT_EQ(1, dinner_cntr.counter);
	ASSERT_EQ(1, tea_cntr.counter);

	ASSERT_EQ(1, head_cntr.counter);
	ASSERT_EQ(0, lunch_cntr.counter);
	ASSERT_EQ(1, dinner_cntr.counter);
	ASSERT_EQ(1, tea_cntr.counter);

}

TEST_F(AlarmInsert, activate_happy)
{
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, dinner));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, tea_time));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, lunch));


	sched::Time time = sched::Time(23, 59, sched::MON);
	AlarmList::checkList(head_alarm, time);
	AlarmList::activateList(head_alarm);
	AlarmList::checkList(head_alarm, time);

	ASSERT_EQ(2, head_cntr.counter);
	ASSERT_EQ(2, lunch_cntr.counter);
	ASSERT_EQ(2, dinner_cntr.counter);
	ASSERT_EQ(2, tea_cntr.counter);

}

void fail_cb()
{
	ASSERT_TRUE(false);
}

void ok_cb()
{
	ASSERT_TRUE(true);
}


TEST_F(AlarmInsert, check_happy)
{
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, dinner));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, tea_time));
	ASSERT_EQ(true, AlarmList::insertNext(head_alarm, lunch));

	sched::Time time = sched::Time(13, 56, sched::MON);
	AlarmList::checkList(head_alarm, time);

	ASSERT_EQ(1, head_cntr.counter);
	ASSERT_EQ(0, lunch_cntr.counter);
	ASSERT_EQ(0, dinner_cntr.counter);
	ASSERT_EQ(0, tea_cntr.counter);


	time.hour = 15;
	time.min = 0;

	AlarmList::checkList(head_alarm, time);
	ASSERT_EQ(1, head_cntr.counter);
	ASSERT_EQ(1, lunch_cntr.counter);
	ASSERT_EQ(0, dinner_cntr.counter);
	ASSERT_EQ(0, tea_cntr.counter);

	time.hour = 19;
	time.min = 0;

	AlarmList::checkList(head_alarm, time);
	ASSERT_EQ(1, head_cntr.counter);
	ASSERT_EQ(1, lunch_cntr.counter);
	ASSERT_EQ(1, dinner_cntr.counter);
	ASSERT_EQ(0, tea_cntr.counter);

	time.hour = 23;
	time.min = 59;

	AlarmList::checkList(head_alarm, time);
	ASSERT_EQ(1, head_cntr.counter);
	ASSERT_EQ(1, lunch_cntr.counter);
	ASSERT_EQ(1, dinner_cntr.counter);
	ASSERT_EQ(1, tea_cntr.counter);
}





