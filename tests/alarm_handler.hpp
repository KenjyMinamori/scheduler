/*
 * alarm_handler.hpp
 *
 *  Created on: Jan 5, 2020
 *      Author: misha
 */

#ifndef ALARM_HANDLER_HPP_
#define ALARM_HANDLER_HPP_

#include "alarm_list.h"

class AlarmTest : public SchedEvent
{
public:
	~AlarmTest(){};

	int counter = 0;

	void action()
	{
		counter++;
	}
};


#endif /* ALARM_HANDLER_HPP_ */
