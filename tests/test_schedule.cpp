/*
 * test_schedule.cpp
 *
 *  Created on: Jan 1, 2020
 *      Author: misha
 */

#include "scheduler.h"
#include <gtest/gtest.h>


#include "alarm_handler.hpp"

static void get_time(sched::Time & time);

Scheduler schedule(get_time);
sched::Time current;

static void get_time(sched::Time & time)
{
	time = current;
}


TEST(new_day, happy) {
	current.day = sched::TUE;
	ASSERT_EQ(true, schedule.check_time());
	ASSERT_EQ(true, schedule.check_begin_of_day());

	for (int i = sched::MON; i <= sched::SUN; i++)
	{
		current.day = (sched::day_t)i;
		ASSERT_EQ(true, schedule.check_time());
		ASSERT_EQ(true, schedule.check_begin_of_day());
	}
}

TEST(new_day, nothing_new) {
	schedule.check_time();
	ASSERT_EQ(false, schedule.check_begin_of_day());

	current.min = 14;
    schedule.check_time();

    ASSERT_EQ(false, schedule.check_begin_of_day());
    current.hour = 10;
    schedule.check_time();

    ASSERT_EQ(false, schedule.check_begin_of_day());
}


TEST(get_time, happy) {
	current.min = 0;
	current.hour = 0;
	current.day = sched::MON;
    ASSERT_EQ(true, schedule.get_time());

    for (int i = 0; i < 60; i++)
    {
    	current.min = i;
    	ASSERT_EQ(true, schedule.get_time());
    }

    for (int i = 0; i < 24; i ++)
    {
    	current.hour = i;
    	ASSERT_EQ(true, schedule.get_time());
    }

    for (int i = (int)sched::MON; i <= (int)sched::SUN; i++)
    {
    	current.day = (sched::day_t)i;
    	ASSERT_EQ(true, schedule.get_time());
    }
}

TEST(get_time, bad) {
	current.min = -1;
	current.hour = 0;
	current.day = sched::MON;
    ASSERT_EQ(false, schedule.get_time());

	current.min = 60;
	current.hour = 0;
	current.day = sched::MON;
    ASSERT_EQ(false, schedule.get_time());

	current.min = 120;
	current.hour = 0;
	current.day = sched::MON;
    ASSERT_EQ(false, schedule.get_time());

	current.min = 0;
	current.hour = 0;
	current.day = sched::UNKNOWN;
    ASSERT_EQ(false, schedule.get_time());

	current.min = 0;
	current.hour = 0;
	current.day = (sched::day_t)((int)sched::SUN + 1);
    ASSERT_EQ(false, schedule.get_time());

	current.min = 0;
	current.hour = -1;
	current.day = sched::MON;
    ASSERT_EQ(false, schedule.get_time());

	current.min = 0;
	current.hour = 24;
	current.day = sched::MON;
    ASSERT_EQ(false, schedule.get_time());
}


using namespace sched;

sched::Time cur;
void get_time2(sched::Time & time)
{
	time = cur;
}



class SchedulerMainTest : public ::testing::Test
{
protected:
	Scheduler my_sched = Scheduler(get_time2);

	AlarmTest fitness2;
	AlarmTest fitness4;
	AlarmTest fitness6;

	AlarmTest glee3;
	AlarmTest glee5;

	AlarmTest wakeup1;
	AlarmTest wakeup2;
	AlarmTest wakeup3;
	AlarmTest wakeup4;
	AlarmTest wakeup5;
	AlarmTest wakeup6;
	AlarmTest wakeup7;

	Alarm al_fitness2;
	Alarm al_fitness4;
	Alarm al_fitness6;

	Alarm al_glee3;
	Alarm al_glee5;

	Alarm al_wakeup1;
	Alarm al_wakeup2;
	Alarm al_wakeup3;
	Alarm al_wakeup4;
	Alarm al_wakeup5;
	Alarm al_wakeup6;
	Alarm al_wakeup7;

	void SetUp()
	{
		cur.day = sched::MON;
		cur.min = 23;
		cur.hour = 0;
		my_sched.start();

		al_fitness2 = Alarm(16, 40, TUE, fitness2);
		al_fitness4 = Alarm(16, 10, THU, fitness4);
		al_fitness6 = Alarm(10, 40, SAT, fitness6);

		al_glee3 = Alarm(19, 30, WED, glee3);
		al_glee5 = Alarm(19, 40, FRI, glee5);

		al_wakeup1 = Alarm(8, 0, MON, wakeup1);
		al_wakeup2 = Alarm(8, 0, TUE, wakeup2);
		al_wakeup3 = Alarm(8, 0, WED, wakeup3);
		al_wakeup4 = Alarm(8, 0, THU, wakeup4);
		al_wakeup5 = Alarm(8, 0, FRI, wakeup5);

		al_wakeup6 = Alarm(9, 30, SAT, wakeup6);
		al_wakeup7 = Alarm(9, 30, SUN, wakeup7);

		ASSERT_EQ(true, my_sched.add_alarm(al_fitness2));
		ASSERT_EQ(true, my_sched.add_alarm(al_fitness4));
		ASSERT_EQ(true, my_sched.add_alarm(al_fitness6));
		ASSERT_EQ(true, my_sched.add_alarm(al_glee3));
		ASSERT_EQ(true, my_sched.add_alarm(al_glee5));
		ASSERT_EQ(true, my_sched.add_alarm(al_wakeup1));
		ASSERT_EQ(true, my_sched.add_alarm(al_wakeup2));
		ASSERT_EQ(true, my_sched.add_alarm(al_wakeup3));
		ASSERT_EQ(true, my_sched.add_alarm(al_wakeup4));
		ASSERT_EQ(true, my_sched.add_alarm(al_wakeup5));
		ASSERT_EQ(true, my_sched.add_alarm(al_wakeup6));
		ASSERT_EQ(true, my_sched.add_alarm(al_wakeup7));

	}

	void TearDown()
	{

	}


};

TEST_F(SchedulerMainTest, start_happy)
{
	ASSERT_EQ(false, my_sched.halted);
}

TEST_F(SchedulerMainTest, add_remove_alarm)
{
	ASSERT_EQ(true, my_sched.remove_alarm(al_fitness2));
	ASSERT_EQ(true, my_sched.add_alarm(al_fitness2));
	ASSERT_EQ(true, my_sched.remove_alarm(al_fitness2));
	ASSERT_EQ(false, my_sched.remove_alarm(al_fitness2));
}

TEST_F(SchedulerMainTest, loop_repeat_the_same)
{
	cur.day = MON;
	cur.hour = 14;
	cur.min = 0;
	ASSERT_EQ(true, my_sched.loop());
	ASSERT_EQ(true, my_sched.loop());
	ASSERT_EQ(true, my_sched.loop());
	ASSERT_EQ(true, my_sched.loop());

	ASSERT_EQ(0, fitness2.counter);
	ASSERT_EQ(0, fitness4.counter);
	ASSERT_EQ(0, fitness6.counter);
	ASSERT_EQ(0, glee3.counter);
	ASSERT_EQ(0, glee5.counter);
	ASSERT_EQ(1, wakeup1.counter);
	ASSERT_EQ(0, wakeup2.counter);
	ASSERT_EQ(0, wakeup3.counter);
	ASSERT_EQ(0, wakeup4.counter);
	ASSERT_EQ(0, wakeup5.counter);
	ASSERT_EQ(0, wakeup6.counter);
	ASSERT_EQ(0, wakeup7.counter);
}

TEST_F(SchedulerMainTest, loop)
{
	cur.day = MON;
	cur.hour = 14;
	cur.min = 0;
	ASSERT_EQ(true, my_sched.loop());

	ASSERT_EQ(0, fitness2.counter);
	ASSERT_EQ(0, fitness4.counter);
	ASSERT_EQ(0, fitness6.counter);
	ASSERT_EQ(0, glee3.counter);
	ASSERT_EQ(0, glee5.counter);
	ASSERT_EQ(1, wakeup1.counter);
	ASSERT_EQ(0, wakeup2.counter);
	ASSERT_EQ(0, wakeup3.counter);
	ASSERT_EQ(0, wakeup4.counter);
	ASSERT_EQ(0, wakeup5.counter);
	ASSERT_EQ(0, wakeup6.counter);
	ASSERT_EQ(0, wakeup7.counter);

	cur.day = TUE;
	cur.hour = 14;
	cur.min = 0;
	ASSERT_EQ(true, my_sched.loop());
	ASSERT_EQ(0, fitness2.counter);
	ASSERT_EQ(0, fitness4.counter);
	ASSERT_EQ(0, fitness6.counter);
	ASSERT_EQ(0, glee3.counter);
	ASSERT_EQ(0, glee5.counter);
	ASSERT_EQ(1, wakeup1.counter);
	ASSERT_EQ(1, wakeup2.counter);
	ASSERT_EQ(0, wakeup3.counter);
	ASSERT_EQ(0, wakeup4.counter);
	ASSERT_EQ(0, wakeup5.counter);
	ASSERT_EQ(0, wakeup6.counter);
	ASSERT_EQ(0, wakeup7.counter);

	cur.day = TUE;
	cur.hour = 16;
	cur.min = 40;
	ASSERT_EQ(true, my_sched.loop());
	ASSERT_EQ(1, wakeup1.counter);
	ASSERT_EQ(1, wakeup2.counter);
	ASSERT_EQ(1, fitness2.counter);
	ASSERT_EQ(0, wakeup3.counter);
	ASSERT_EQ(0, glee3.counter);
	ASSERT_EQ(0, wakeup4.counter);
	ASSERT_EQ(0, fitness4.counter);
	ASSERT_EQ(0, wakeup5.counter);
	ASSERT_EQ(0, glee5.counter);
	ASSERT_EQ(0, wakeup6.counter);
	ASSERT_EQ(0, fitness6.counter);
	ASSERT_EQ(0, wakeup7.counter);

	cur.day = (day_t)3;
	cur.hour = 23;
	cur.min = 40;
	ASSERT_EQ(true, my_sched.loop());
	ASSERT_EQ(1, wakeup1.counter);
	ASSERT_EQ(1, wakeup2.counter);
	ASSERT_EQ(1, fitness2.counter);
	ASSERT_EQ(1, wakeup3.counter);
	ASSERT_EQ(1, glee3.counter);
	ASSERT_EQ(0, wakeup4.counter);
	ASSERT_EQ(0, fitness4.counter);
	ASSERT_EQ(0, wakeup5.counter);
	ASSERT_EQ(0, glee5.counter);
	ASSERT_EQ(0, wakeup6.counter);
	ASSERT_EQ(0, fitness6.counter);
	ASSERT_EQ(0, wakeup7.counter);

	cur.day = (day_t)2;
	cur.hour = 23;
	cur.min = 40;
	ASSERT_EQ(true, my_sched.loop());
	ASSERT_EQ(1, wakeup1.counter);
	ASSERT_EQ(2, wakeup2.counter);
	ASSERT_EQ(2, fitness2.counter);
	ASSERT_EQ(1, wakeup3.counter);
	ASSERT_EQ(1, glee3.counter);
	ASSERT_EQ(0, wakeup4.counter);
	ASSERT_EQ(0, fitness4.counter);
	ASSERT_EQ(0, wakeup5.counter);
	ASSERT_EQ(0, glee5.counter);
	ASSERT_EQ(0, wakeup6.counter);
	ASSERT_EQ(0, fitness6.counter);
	ASSERT_EQ(0, wakeup7.counter);

}
