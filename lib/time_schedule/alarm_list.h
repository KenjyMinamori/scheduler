/*
 * alarm_list.h
 *
 *  Created on: Jan 2, 2020
 *      Author: misha
 */

#ifndef TIME_SCHEDULE_ALARM_LIST_H_
#define TIME_SCHEDULE_ALARM_LIST_H_

#include "time_utils.h"

class SchedEvent{
public:
	virtual void action(){};
	virtual ~SchedEvent(){};
};


class AlarmList;

class Alarm
{
public:
	friend AlarmList;
	Alarm(int hour, int min, sched::day_t day, SchedEvent & hand): handler(&hand){
		time.hour = hour;
		time.min  = min;
		time.day = day;
	};
	Alarm(int hour, int min, sched::day_t day){
		time.hour = hour;
		time.min  = min;
		time.day = day;

	}

	Alarm(){};
	Alarm(const sched::Time& t,SchedEvent& hand): time(t), handler(&hand){};
	sched::Time time = sched::Time(0, 0, sched::MON);

	bool activated = true;

	// Check time and do Action if it should be
	bool check(sched::Time& check_time)
	{
		if (!activated)
			return false;

		if (time.day != check_time.day)
			return false;

		if (check_time < time)
			return false;

		do_action();

		return true;
	}

	Alarm& operator=(const Alarm& clone){
		if (this == &clone)
			return *this;

		time = clone.time;
		activated = clone.activated;
		next = clone.next;
		handler = clone.handler;

		return *this;
	}

	SchedEvent * handler = nullptr;

	void activate()
	{
		activated = true;
	}
#ifndef TEST
private:
#endif
	void do_action()
	{
		if (handler) handler->action();
		activated = false;
	}

	Alarm * next = 0;

};


class AlarmList
{
public:
	// Insert in asceding order
	static bool insertNext(Alarm & head, Alarm & next_ptr);
	static bool checkList(Alarm & head, sched::Time& time);
	static bool activateList(Alarm & head);
	static bool remove(Alarm & head, Alarm & next_ptr);
};

typedef Alarm* AlarmPtr;

#ifndef ALARM_BUFFER_SIZE
#warning "Define alarm buffer size!"
#define ALARM_BUFFER_SIZE 100
#endif //ALARM_BUFFER_SIZE

struct AlarmBuffer
{
	Alarm buffer [ALARM_BUFFER_SIZE] = {};
	const int size = ALARM_BUFFER_SIZE;
	bool mem[ALARM_BUFFER_SIZE] = {};

	bool clone_alarm(AlarmPtr& alarm, Alarm& clone)
	{
		int cur = get_cursor();

		//Is buffer full?
		if (cur < 0 ) return false;

		buffer[cur] = clone;

		alarm = &buffer[cur];
		mem[cur] = true;
		return true;
	}

	bool delete_alarm(AlarmPtr& alarm){
		for (int i = 0; i < size; i++)
		{
			if (&buffer[i] == alarm)
			{
				mem[i] = false;
				return true;
			}
		}
		return false;
	}

	int get_cursor(){
		int i = 0;
		while(mem[i]){
			i++;

			//Out of range?
			if (i >= size) return -1;
		}


		return i;
	}
};

#endif /* TIME_SCHEDULE_ALARM_LIST_H_ */
