/*
 * scheduler.cpp
 *
 *  Created on: Jan 1, 2020
 *      Author: misha
 */
#include "scheduler.h"

using namespace sched;

SchedEvent head_dummy;

bool Scheduler::start()
{
	if (!check_time())
		return false;

	halted = false;
	drop_alarms_for_this_day();

	return true;
}

bool Scheduler::loop()
{
	if (halted)
		return false;


	//get_time
	//if (begin_of_day()) drop_status()
	//if (match_time(event)) do_action(event)

	if (!check_time())
		return false;

	check_begin_of_day();

	check_alarms();

	return true;
}

bool Scheduler::get_time()
{
	sched::Time time;
	get_current_time(time);

	if (time.day < sched::MON || time.day > sched::SUN)
		return false;

	if (time.hour < 0 || time.hour > 23)
		return false;

	if (time.min < 0 || time.min > 59)
		return false;

	current_time = time;
	return true;
}

bool Scheduler::check_time()
{

	if (!get_time())
		return false;


    return true;
}

bool Scheduler::check_begin_of_day()
{
	bool new_day = last_time.day != current_time.day;

	if (last_time.day == sched::UNKNOWN)
		new_day = true;

    if (new_day)
    {
		drop_alarms_for_this_day();
	}

	//Save current time
    last_time = current_time;
    return new_day;
}


void Scheduler::drop_alarms_for_this_day()
{
	//TODO: test
	AlarmList::activateList(alarms[current_time.day]);
}

void Scheduler::check_alarms()
{
	// TODO: test
	AlarmList::checkList(alarms[current_time.day], current_time);
}

void Scheduler::init_alarms()
{
	for (int i = sched::MON; i <= sched::SUN; ++i)
	{
		alarms[i] = Alarm(0, 0,(sched::day_t)i, head_dummy);
	}
}

bool Scheduler::add_alarm(Alarm &alarm)
{
	if (alarm.time.day < sched::MON || alarm.time.day> sched::SUN)
		return false;

	return AlarmList::insertNext(alarms[alarm.time.day], alarm);
}

bool Scheduler::remove_alarm(Alarm &alarm)
{
	return AlarmList::remove(alarms[alarm.time.day], alarm);
}
