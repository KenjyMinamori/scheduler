/*
 * alarm_list.cpp
 *
 *  Created on: Jan 2, 2020
 *      Author: misha
 */

#include "alarm_list.h"

//TODO: DUBLICATS!!!
// Insert in asceding order
bool AlarmList::insertNext(Alarm& head, Alarm& next_ptr)
{
	// If it is first element
	if (!head.next)
	{
		head.next = &next_ptr;
		return true;
	}

	Alarm * cursor = &head;
	while (cursor->next)
	{
		if (cursor->next->time > next_ptr.time)
		{
			next_ptr.next = cursor->next;
			cursor->next = &next_ptr;
			return true;
		}
		cursor = cursor->next;
	}
	cursor->next = &next_ptr;
	return true;
}

bool AlarmList::checkList(Alarm & head, sched::Time& time)
{
	Alarm * cursor = &head;

	while (cursor)
	{
		cursor->check(time);

		cursor = cursor->next;
	}

	return true;
}

bool AlarmList::remove(Alarm & head, Alarm & obj)
{
	Alarm * cursor = &head;

	while (cursor)
	{
		//cursor->check(time);
		if (cursor->next == &obj){
			//Delete it from list
			cursor->next = cursor->next->next;
			return true;
		}
		cursor = cursor->next;
	}

	return false;
}

bool AlarmList::activateList(Alarm & head)
{
	Alarm * cursor = &head;

	while (cursor)
	{
		cursor->activate();

		cursor = cursor->next;
	}
	return true;
}
