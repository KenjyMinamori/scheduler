/*
 * time_utils.h
 *
 *  Created on: Jan 2, 2020
 *      Author: misha
 */

#ifndef TIME_SCHEDULE_TIME_UTILS_H_
#define TIME_SCHEDULE_TIME_UTILS_H_

#include <functional>

namespace sched{


typedef enum {
	UNKNOWN = 0, MON, TUE, WED, THU, FRI, SAT, SUN
} day_t;

struct SchedulerTime {
	day_t day;
	int min;
	int hour;
	bool repeat;
};

struct Time {

	int hour = 0;
	int min = 0;
	day_t day = UNKNOWN;

	Time& operator=(const Time& obj)
	{
		if (this == &obj)
			return *this;

		min = obj.min;
		hour = obj.hour;
		day = obj.day;
		return *this;
	}

	const bool operator>(const Time& obj)
	{
		//TODO: how should we do it?
		if (day != obj.day)
			return false;

		if (hour > obj.hour)
			return true;
		else if (hour < obj.hour)
			return false;

		if (min > obj.min)
			return true;
		else
			return false;
	}

	const bool operator==(const Time& obj)
	{
		if (day != obj.day)
			return false;

		if (hour != obj.hour)
			return false;

		if (min != obj.min)
			return false;
		return true;
	}

	const bool operator>=(const Time& obj)
	{
		return (*this > obj) || (*this == obj);
	}

	const bool operator<(const Time& obj)
	{
		if (*this == obj) return false;
		if (*this > obj) return false;
		return true;
	}

	const bool operator<=(const Time& obj)
	{
		return (*this < obj) || (*this == obj);
	}
	Time(int h, int m, day_t d): hour(h), min(m), day(d){
		validate_time();
	};
	Time(){validate_time();};
	Time(const Time& cpy): hour(cpy.hour), min(cpy.min), day(cpy.day){validate_time();};

	void validate_time()
	{
		if (hour < 0)
			 hour = 0;

		if (hour > 23)
			hour = 23;

		if (min < 0)
			min = 0;

		if (min > 59)
			min = 59;

		if (day < UNKNOWN)
			day = UNKNOWN;

		if (day > SUN)
			day = SUN;
	}

};
	typedef std::function<void(sched::Time & time)> time_getter_t;


}

#endif /* TIME_SCHEDULE_TIME_UTILS_H_ */
