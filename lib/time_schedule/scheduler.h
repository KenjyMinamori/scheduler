/*
 * scheduler.h
 *
 *  Created on: Jan 1, 2020
 *      Author: misha
 */

#ifndef TIME_SCHEDULE_SCHEDULER_H_
#define TIME_SCHEDULE_SCHEDULER_H_

#include "time_utils.h"
#include "alarm_list.h"


class SchedulerMainTest;

class Scheduler {
public:
	friend SchedulerMainTest;
	explicit Scheduler(sched::time_getter_t time_getter): get_current_time(time_getter){
		init_alarms();
	};

	//TODO: what if we add the same element& :0
	bool add_alarm(Alarm &time);
	bool remove_alarm(Alarm &time);
	bool loop();
	bool start();

#ifndef TEST
private:
#endif
	void check_alarms();

	// Alarm lists for the week (separated list for each day)
 	Alarm alarms[8];


	bool halted = true;

	Scheduler(){ init_alarms();};
	sched::time_getter_t get_current_time;
	sched::Time last_time;
	sched::Time current_time;

 	void init_alarms();

	bool check_begin_of_day();
	bool check_time();
	bool get_time();

protected:
	void drop_alarms_for_this_day();

};

#endif /* TIME_SCHEDULE_SCHEDULER_H_ */
