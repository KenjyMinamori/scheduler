//============================================================================
// Name        : time_sheduler.cpp
// Author      : Mikhail Natalenko
// Version     :
// Copyright   : You can use it if you want without any warranty
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "time_glue.h"
#include "scheduler.h"
#include <gtest/gtest.h>


using namespace std;

int main(int argc, char **argv) {
	cout << "Starts here" << endl; // prints Starts here

	cout << "Time: " << get_seconds() << endl;

    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

	//return 0;
}
