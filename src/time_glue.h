/*
 * time_glue.h
 *
 *  Created on: Jan 1, 2020
 *      Author: misha
 */

#ifndef TIME_GLUE_H_
#define TIME_GLUE_H_

#include "time.h"

unsigned int
get_seconds();



#endif /* TIME_GLUE_H_ */
